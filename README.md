# Openpay Fixes

- This project contains syntactic improvements for the version of Swift used.
- A validation has been added for VISA / Mastercard with the minimum number of numeric characters allowed to be a valid card number.
- A new target has been added to generate a universal binary so that the project can support several architectures.

The original source code is avaiblable in: https://github.com/open-pay/openpay-swift-ios